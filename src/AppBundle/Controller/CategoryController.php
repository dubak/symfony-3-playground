<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class CategoryController
 * @package AppBundle\Controller
 */
class CategoryController extends Controller
{
    /**
     * @Route(path="/categories-list/{page}", name="categories-list", requirements={"page": "\d+"})
     * @param int $page
     * @return Response
     */
    public function allAction($page = 1)
    {
        $slug = 'list-default';
        return $this->render('category/all.html.twig', array('name' => $slug, 'page' => $page));
    }

    /**
     * @Route(path="/categories-list/", name="categories-list-redirect")
     * @return Response
     */
    public function allRedirectsAction()
    {
        return $this->redirectToRoute('categories-list', []);
    }

    /**
     * @Route(
     *  name="category-list",
     *  path="/category/{alias}/{page}",
     *  requirements={
     *      "alias": "[a-zA-Z0-9\-]+",
     *      "page": "\d+"
     *  },
     *  defaults={
     *      "_controller": "AppBundle:Category:list"}
     * )
     * @param $alias
     * @param int $page
     * @return Response
     */
    public function listAction($alias, $page = 1)
    {
        return $this->render('category/list.html.twig', array('name' => $alias,'page' => $page));
    }

    /**
     * @Route(
     *  name="category-list-redirect",
     *  path="/category/{alias}/",
     *  requirements={
     *      "alias": "[a-zA-Z0-9\-]+"},
     *  defaults={
     *      "_controller": "AppBundle:Category:listRedirect"}
     * )
     * @param $alias
     * @return Response
     */
    public function listRedirectAction($alias)
    {
        return $this->redirectToRoute('category-list', ['alias'=>$alias]);
    }
}