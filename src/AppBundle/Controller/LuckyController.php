<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class LuckyController
 * @package AppBundle\Controller
 */
class LuckyController extends Controller
{
    /**
     * @Route("/lucky/", name="lucky-index")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        return $this->render('lucky/index.html.twig', array('name' => 'param-default'));
    }


    /**
     * @Route("/lucky/number/", name="lucky-number")
     * @Method({"GET"})
     */
    public function numberAction()
    {
        $number = mt_rand(0, 100);

        return new Response(
            '<html><body>
                Lucky number: '.$number.'<br />
                <a href="'.$this->generateUrl('lucky-index', [], UrlGeneratorInterface::ABSOLUTE_URL).'">Lucky home</a><br />
            </body></html>'
        );
    }

    /**
     * @Route("/lucky/{slug}/", name="lucky-slug", requirements={"slug":"[a-zA-Z]+"} )
     * @Method({"GET"})
     */
    public function nameAction($slug)
    {
        return $this->render('lucky/name.html.twig', array('name' => $slug));
    }

    /**
     * @Route("/lucky/{slug}/{page}/", name="lucky-slug-page", requirements={"slug":"[a-zA-Z]+"} )
     * @Method({"GET"})
     */
    public function namePageAction(Request $request, $slug, $page = 1)
    {
        $isAjax = $request->isXmlHttpRequest(); // is it an Ajax request?

        // retrieve GET and POST variables respectively
        $getParamSort = $request->query->get('sort');

        return $this->render('lucky/name.html.twig', array(
            'name' => $slug, 'page' => $page,
            'isAjax'=>$isAjax, 'getParamSort' => $getParamSort
        ));
    }
}